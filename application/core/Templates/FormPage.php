<!DOCTYPE html>
<html>
<head>
    <title> <?php if (isset($title)) echo $title?></title>
    <link href="css/main.css" rel="stylesheet" type="text/css">
</head>
<body>
<h1 class='PageName'>Product Add</h1>



<script src='http://code.jquery.com/jquery-latest.js'></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.0/jquery.validate.min.js"></script>
<form id="product_form" action="/" method="POST">


    <div class="buttons">
            <input type="submit" value="Save" class="up">
            <a href="http://mvctask/" class="btn">Cancel</a>
            <div class="hrLine2">
                <hr class="hrLine" style="border: 2px solid;">
            </div>


    <label>SKU </label>
    <input type="text" class='productData' id="sku" name="SKU" maxlength="30" class='required'>
    <br>
    <label>Name </label>
    <input type="text" class='productData' id="name" name="Name" maxlength="30" required>
    <br>
    <label>Price ($) </label>
    <input type="number" class='productData' min=0 step="0.01" id="price" name="Price" required>
    <br>
    <label>Type Switcher</label>
    <select class='productData' id="productType" name="Category">
        <option id="TypeSwitcher" value="typeSwitcher">Type Switcher</option>
        <option id="DVD" value="DVD">DVD</option>
        <option id="Furniture" value="Furniture">Furniture</option>
        <option id="Book" value="Book">Book</option>
    </select>
    <div id='DVD-params' class='atributs'>
        <label>Size (MB)</label>
        <input type="number" id="size" name="Size" min=0 value="0">
        <br>
        <p>Please, provide size</p>
    </div>
    <div id='Furniture-params' class='atributs'>
        <label>Height (CM)</label>
        <input style="margin: 5px;" type="number" id="height" name="Height" min=0 max=300 value="0">
        <br>
        <label>Width (CM)</label>
        <input style="margin: 5px;" type="number" id="width" name="Width" min=0 max=300 value="0">
        <br>
        <label>Lenght (CM)</label>
        <input style="margin: 5px;" type="number" id="length" name="Length" min=0 max=300 value="0">
        <br>
        <p>Please, provide dimention</p>
    </div>
    <div id='Book-params' class='atributs'>
        <label>Weight (KG)</label>
        <input type="number" id="weight" name="Weight" min=0 value="0">
        <br>
        <p>Please, provide weight</p>
    </div>
</form>
<script>
    $("#productType").change(function() {
        $('.atributs').hide();
        $('#' + $(this).val() + '-params').show();
    });

    $("#product_form").validate({
        rules: {
            SKU: {
                required: true,
                remote: {
                    url: "/checkSku",
                    type: "POST"
                }
            }
        },
        messages: {
            SKU: {
                required: "Please Enter sku!",
                remote: "Sku already in use!"
            }
        }
    });
</script>

</div>
</body>
</html>





<!--<!DOCTYPE html>-->
<!--<html>-->
<!--<head>-->
<!--    <title> --><?php //if (isset($title)) echo $title?><!--</title>-->
<!--    <link href="css/main.css" rel="stylesheet" type="text/css">-->
<!--</head>-->
<!--<body>-->
<!--<h1 class='PageName'>Product Add</h1>-->
<!---->
<!--<div class="buttons">-->
<!--    <form action="http://mvctask/" id='product_form' method="post">-->
<!--        <input type="submit" value="Save" class="up">-->
<!--        <a href="http://mvctask/" class="btn">Cancel</a>-->
<!--        <div class="hrLine2">-->
<!--            <hr class="hrLine" style="border: 2px solid;">-->
<!--        </div>-->
<!--        <label>SKU</label>-->
<!--        <input type="text" id='SKU' name="SKU" placeholder="Write sku">-->
<!--        <br><br>-->
<!--        <label>Name </label>-->
<!--        <input type="text" id='Name' name="Name" placeholder="Write name">-->
<!--        <br><br>-->
<!--        <label>Price ($) </label>-->
<!--        <input type="text" id='Price' name="Price" placeholder="Write price ($)">-->
<!--        <br><br>-->
<!---->
<!---->
<!--        <label>Category </label>-->
<!--        <input type="text" id='Category' name="Category" placeholder="Write category">-->
<!--        <br><br>-->
<!---->
<!--        <label>Weight</label>-->
<!--        <input type="text" id='Weight' name="Weight" placeholder="Write weight">-->
<!--        <br><br>-->
<!---->
<!--        <label>Size</label>-->
<!--        <input type="text" id='Size' name="Size" placeholder="Write weight">-->
<!--        <br><br>-->
<!---->
<!--        <label>Width</label>-->
<!--        <input type="text" id='Weight' name="Width" placeholder="Write weight">-->
<!--        <br><br>-->
<!---->
<!--        <label>Height</label>-->
<!--        <input type="text" id='Height' name="Height" placeholder="Write weight">-->
<!--        <br><br>-->
<!---->
<!--        <label>Length</label>-->
<!--        <input type="text" id='Length' name="Length" placeholder="Write weight">-->
<!--        <br><br>-->
<!--                        <label>Type Switcher </label>-->
<!--                        <select name='list' id='Category'>-->
<!--                            <option value="" hidden>Type Switcher</option>-->
<!--                            <option value="dvd">DVD-disc</option>-->
<!--                            <option value="book">Book</option>-->
<!--                            <option value="furniture">Furniture</option>-->
<!--                        </select>-->
<!---->
<!--                        <div id ="dvd" style="display: none" class="data">-->
<!--                            <br>-->
<!--                            <label>Size (MB) </label>-->
<!--                            <input type="text" id='size' name="size" placeholder="Write size">-->
<!--                            <p id="dvd">Please, provide disc space in MB</p>-->
<!--                        </div>-->
<!---->
<!--                        <div id ="furniture" style="display: none" class="data">-->
<!--                            <br>-->
<!--                            <label>Height (CM) </label>-->
<!--                            <input type="text" id='height' name="height" placeholder="Write height">-->
<!--                            <br><br>-->
<!--                            <label>Width (CM) </label>-->
<!--                            <input type="text" id='width' name="width" placeholder="Write width">-->
<!--                            <br><br>-->
<!--                            <label>Length (CM) </label>-->
<!--                            <input type="text" id='length' name="length" placeholder="Write length">-->
<!--                            <p>Please, provide furniture height, width, length in CM</p>-->
<!--                        </div>-->
<!---->
<!--                        <div id ="book" style="display: none" class="data">-->
<!--                            <br>-->
<!--                            <label>Weight (KG) </label>-->
<!--                            <input type="text" id='weight' name="weight" placeholder="Write size">-->
<!--                            <p>Please, provide book weight in KG</p>-->
<!--                        </div>-->
<!---->
<!--    </form>-->
<!---->
<!--</div>-->
<!--</div>-->
<!--</body>-->
<!--</html>-->